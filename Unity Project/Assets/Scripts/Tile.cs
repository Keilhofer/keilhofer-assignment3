﻿using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour
{
    //Script that is on the tile prefab (spawned at runtime)

    private GameManager m_manager;
    public GameManager.Symbols Symbol { get; private set; }
    private Transform m_child;
    private Collider m_collider;

    //Set a variable for the child of each card holder and the colliders
    private void Start()
    {
        m_child = this.transform.GetChild( 0 );
        m_collider = GetComponent<Collider>();
    }

    //the tile selected with the mouse is the current tile
    private void OnMouseDown()
    {
        m_manager.TileSelected( this );
    }

    //Start the game manager's actions and apply symbols
    public void Initialize( GameManager manager, GameManager.Symbols symbol )
    {
        m_manager = manager;
        Symbol = symbol;
    }

    //Reveal tiles by stopping all other coroutines and playing the one that spins the cards around, disable the colliders
    public void Reveal()
    {
        StopAllCoroutines();
        StartCoroutine( Spin( 180, 0.8f ) );
        m_collider.enabled = false;
    }

    //Hide the tiles by stopping all other coroutines and playing the one that spins the cards back the other way, re-enable the colliders
    public void Hide()
    {
        StopAllCoroutines();
        StartCoroutine( Spin( 0, 0.8f ) );
        m_collider.enabled = true;
    }

    //Coroutine that spins the cards when called upon
    private IEnumerator Spin( float target, float time )
    {
        float timer = 0;
        float startingRotation = m_child.eulerAngles.y;
        Vector3 euler = m_child.eulerAngles;
        while ( timer < time )
        {
            timer += Time.deltaTime;
            euler.y = Mathf.LerpAngle( startingRotation, target, timer / time );
            m_child.eulerAngles = euler;
            yield return null;
        }
        euler.y = target;
        m_child.eulerAngles = euler;
    }
}
